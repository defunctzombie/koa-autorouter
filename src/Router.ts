import fs from 'fs';
import path from 'path';
import compose from 'koa-compose';

import Route from './Route';
import { Middleware } from 'koa';

interface RouterOptions {
    root: string;
    ignorePattern?: string;
}

class Router {
    private rootPath: string;
    private ignorePattern?: string;

    constructor(options: RouterOptions) {
        this.rootPath = path.normalize(options.root);
        this.ignorePattern = options.ignorePattern;
    }

    middleware() {
        return this.loadRoutes(this.rootPath);
    }

    // recursively load routes from a basepath
    // return a middleware
    private loadRoutes(loadPath: string): Middleware {
        const fns = [];
        const directoryItems = fs.readdirSync(loadPath);

        // turns /path/to/routes -> ''
        // turns /path/to/routes/folder -> '/folder'
        let base = loadPath.replace(this.rootPath, '');
        if (base[0] !== '/') {
            base = '/' + base;
        }

        // sort parameter paths last
        directoryItems.sort(ParamCompare);

        for (const dirItem of directoryItems) {
            if (this.ignorePattern && new RegExp(this.ignorePattern).test(dirItem)) {
                continue;
            }

            const fullDirItemPath = path.join(loadPath, dirItem);
            const stat = fs.statSync(fullDirItemPath);

            // recurse into the directory
            if (stat.isDirectory()) {
                const handler = this.loadRoutes(fullDirItemPath);
                fns.push(handler);
                continue;
            }

            const module = require(fullDirItemPath);
            const defaultExport = module.default;
            if (typeof defaultExport !== 'function') {
                throw new Error(
                    `${fullDirItemPath} should default export a function that accepts a Route instance`,
                );
            }

            // item name without extension
            const baseDirItem: string = dirItem.slice(0, -path.extname(dirItem).length);
            const child = baseDirItem === 'index' ? '' : '/' + baseDirItem;
            const routePath: string = base + child;

            // turn `[paramName]` into :paramName for use with PathMatch
            const parameterized: string = ('' + routePath).replace(
                /\[(\S+?)\]/g,
                (_match: string, p1: string) => ':' + p1,
            );

            const normalized = path.normalize(parameterized);
            const route = new Route(normalized);

            // default export is a function that accepts a route instance
            defaultExport(route);
            fns.push(route.handler());
        }

        return compose(fns);
    }
}

/** Compare function to sort array elements.
 *  Elements that look like parameters (i.e. [foo]) are sorted _after_
 *  elements which are not parameters.
 */
function ParamCompare(a: string, b: string): number {
    // a is a param, b is not -> b sorts before a
    if (a[0] === '[' && b[0] !== '[') {
        return 1;
    }
    // a is not a param, b is a param -> a sorts before b
    else if (a[0] !== '[' && b[0] === '[') {
        return -1;
    }

    // both are either params or not params, use alpha sorting
    if (a < b) {
        return -1;
    } else if (a > b) {
        return 1;
    }
    return 0;
}

export default Router;
