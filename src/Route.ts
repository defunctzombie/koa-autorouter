import Koa, { Middleware } from 'koa';
import compose from 'koa-compose';
import { match as PathMatch } from 'path-to-regexp';

type Request<ParamsT = {}> = Koa.Request & { params: ParamsT };
type Response = Koa.Response;
type Next = () => Promise<any>;

type UseHandler<Params> = (req: Request<Params>, res: Response, next: Next) => Promise<void>;
type VerbHandler<Params> = (req: Request<Params>, res: Response) => Promise<void>;

class Route<Params = {}> {
    private matcher: ReturnType<typeof PathMatch>;
    private prefixMatcher: ReturnType<typeof PathMatch>;
    private verbs: Middleware[] = [];
    private middleware: Middleware[] = [];

    /** construct a new route on the given route path */
    constructor(routePath: string) {
        const matcher = PathMatch(routePath, { decode: decodeURIComponent });
        const prefixMatcher = PathMatch(routePath, { end: false, decode: decodeURIComponent });
        this.matcher = matcher;
        this.prefixMatcher = prefixMatcher;
    }

    /** return middleware to handle the route */
    handler(): Middleware {
        const finalMiddleware: Middleware[] = [];

        if (this.verbs.length > 0) {
            const matcher = this.matcher;
            const handler = compose(this.verbs);
            finalMiddleware.push(
                (ctx, next): Promise<any> => {
                    const matchResult = matcher(ctx.path);
                    if (matchResult === false) {
                        return next();
                    }

                    (ctx.request as Request).params = matchResult.params;
                    return handler(ctx, next);
                },
            );
        }

        if (this.middleware.length > 0) {
            const matcher = this.prefixMatcher;
            const handler = compose(this.middleware);
            finalMiddleware.push(
                (ctx, next): Promise<any> => {
                    const matchResult = matcher(ctx.path);
                    if (matchResult === false) {
                        return next();
                    }

                    (ctx.request as Request).params = matchResult.params;
                    return handler(ctx, next);
                },
            );
        }

        return compose(finalMiddleware);
    }

    /** add `fn` as middleware for the route */
    use(fn: UseHandler<Params>) {
        this.middleware.push((ctx, next) => {
            return fn(ctx.request as Request<Params>, ctx.response, next);
        });
    }

    head(fn: VerbHandler<Params>) {
        this.addVerbHandler('HEAD', fn);
    }

    options(fn: VerbHandler<Params>) {
        this.addVerbHandler('OPTIONS', fn);
    }

    get(fn: VerbHandler<Params>) {
        this.addVerbHandler('GET', fn);
    }

    patch(fn: VerbHandler<Params>) {
        this.addVerbHandler('PATCH', fn);
    }

    put(fn: VerbHandler<Params>) {
        this.addVerbHandler('PUT', fn);
    }

    post(fn: VerbHandler<Params>) {
        this.addVerbHandler('POST', fn);
    }

    delete(fn: VerbHandler<Params>) {
        this.addVerbHandler('DELETE', fn);
    }

    // helper to add a `fn` as a handler for the particular http verb
    private addVerbHandler(verb: string, fn: VerbHandler<Params>) {
        this.verbs.push((ctx, next) => {
            if (ctx.method !== verb) {
                return next();
            }

            return fn(ctx.request as Request<Params>, ctx.response);
        });
    }
}

export default Route;
export { Request, Response, Next };
