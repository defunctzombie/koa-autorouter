import request from 'supertest';
import Koa from 'koa';

import Router from 'src/Router';

const testRoutesRoot = `${__dirname}/../../test-routes`;

test('peer to param route', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    const response = await request(app.callback())
        .get('/params/peer')
        .expect(200);
    expect(response.body).toBe('peer path');
});
