import { Route } from 'src';

export default (route: Route<{ param: string }>) => {
    route.get(async (req, res) => {
        const { param } = req.params;
        res.type = 'application/json';
        res.body = JSON.stringify(`param:${param}`);
    });
};
