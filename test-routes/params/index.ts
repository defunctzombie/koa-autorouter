import { Route } from 'src';

export default (route: Route) => {
    route.post(async (_, res) => {
        res.type = 'application/json';
        res.body = JSON.stringify('post index');
    });
};
