import request from 'supertest';
import Koa from 'koa';

import Router from 'src/Router';

const testRoutesRoot = `${__dirname}/../../test-routes`;

test('nested post', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    const response = await request(app.callback())
        .post('/params')
        .expect(200);
    expect(response.body).toBe('post index');
});
