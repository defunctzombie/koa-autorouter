import { Route } from 'src';

export default (route: Route<{ lava: string; flow: string }>) => {
    route.get(async (req, res) => {
        const { lava, flow } = req.params;
        res.type = 'application/json';
        res.body = JSON.stringify(`param.${lava}.${flow}`);
    });
};
