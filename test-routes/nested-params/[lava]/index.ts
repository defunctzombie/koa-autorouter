import { Route } from 'src';

export default (route: Route<{ lava: string }>) => {
    route.get(async (req, res) => {
        const { lava } = req.params;
        res.type = 'application/json';
        res.body = JSON.stringify(`param.${lava}`);
    });
};
