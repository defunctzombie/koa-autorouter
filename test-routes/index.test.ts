import request from 'supertest';
import Koa from 'koa';

import Router from 'src/Router';

const testRoutesRoot = `${__dirname}/../test-routes`;

test('root index', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    const response = await request(app.callback())
        .get('/')
        .expect(200);
    expect(response.body).toBe('index');
});

test('404', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    await request(app.callback())
        .get('/does-not-exist')
        .expect(404);
});

test('404 valid route unsupported method', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    await request(app.callback())
        .post('/')
        .expect(404);
});
