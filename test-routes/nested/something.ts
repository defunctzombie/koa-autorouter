import { Route } from 'src';

export default (route: Route) => {
    route.get(async (req, res) => {
        res.type = 'application/json';
        res.body = JSON.stringify(req.path);
    });
};
