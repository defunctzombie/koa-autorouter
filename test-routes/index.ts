import { Route } from 'src';

export default (route: Route) => {
    route.get(async (_, res) => {
        res.type = 'application/json';
        res.body = JSON.stringify('index');
    });
};
