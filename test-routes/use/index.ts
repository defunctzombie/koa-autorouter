import { Route } from 'src';

export default (route: Route) => {
    route.use(async (_, res, next) => {
        res.body = { foo: 'bar' };
        return next();
    });
};
