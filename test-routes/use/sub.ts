import { Route } from 'src';

export default (route: Route) => {
    route.get(async (_, res) => {
        const val = (res.body as any).foo;
        res.body = `"${val}:baz"`;
    });
};
