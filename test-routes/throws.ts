import { Route } from 'src';

export default (route: Route) => {
    route.get(async (_, res) => {
        res.ctx.throw(400, 'custom message');
    });
};
