import request from 'supertest';
import Koa from 'koa';

import Router from 'src/Router';

const testRoutesRoot = `${__dirname}/../test-routes`;

test('throw', async () => {
    const app = new Koa();

    app.use(async (ctx, next) => {
        try {
            await next();
        } catch (err: any) {
            ctx.response.status = err.status || 500;
            ctx.response.body = {
                message: err.message,
            };
        }
    });
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());

    await request(app.callback()).get('/throws').expect(400, {
        message: 'custom message',
    });
});
