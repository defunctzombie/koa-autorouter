import request from 'supertest';
import Koa from 'koa';

import Router from 'src/Router';

const testRoutesRoot = `${__dirname}/../test-routes`;

test('root 200', async () => {
    const app = new Koa();
    app.use(new Router({ root: testRoutesRoot, ignorePattern: 'test.ts' }).middleware());
    await request(app.callback())
        .get('/200')
        .expect(200);
});
